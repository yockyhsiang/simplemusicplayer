using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using Android.Util;
using Java.IO;
using Java.Util.Regex;

namespace SimpleMusicPlayer.Droid
{
    [Service]
    public class MusicPlayerService : Service, MediaPlayer.IOnPreparedListener, MediaPlayer.IOnCompletionListener
    {

        public static Boolean isServiceRunning;
        public const string ACTION_PLAY = "com.simplemusicplayer.action.PLAY_TEST";
        public const string ACTION_PAUSE = "com.simplemusicplayer.action.PAUSE";
        public const string ACTION_RESUME = "com.simplemusicplayer.action.RESUME";
        public const string ACTION_PLAY_DIRECTORY = "com.simplemusicplayer.action.PLAY_DIRECTORY";
        public const string ACTION_PREVIOUS = "com.simplemusicplayer.action.PREVIOUS";
        public const string ACTION_NEXT = "com.simplemusicplayer.action.NEXT";
        public const string FILE_PATTERN = "(.*)\\.(mp3|flac)$";

        private static MediaPlayer mMediaPlayer;

        public List<Music> filesQueue = new List<Music>();

        public int currentlyPlaying = -1;

        // Binder given to clients
        private IBinder mBinder;


        [return: GeneratedEnum]
        public override StartCommandResult OnStartCommand(Intent intent, [GeneratedEnum] StartCommandFlags flags, int startId)
        {

            switch (intent.Action)
            {
                case ACTION_PLAY:
                    release();
                    mMediaPlayer = MediaPlayer.Create(this, Resource.Raw.test);
                    mMediaPlayer.SetOnPreparedListener(this);
                    break;
                case ACTION_PLAY_DIRECTORY:
                    filesQueue.Clear();
                    queueDirectory(intent.GetStringExtra("directory"));
                    if (filesQueue.Count != 0)
                    {
                        playQueue();
                    }
                    break;
                case ACTION_PAUSE:
                    if (mMediaPlayer != null)
                    {
                        mMediaPlayer.Pause();
                    }
                    break;
                case ACTION_RESUME:
                    if (mMediaPlayer != null)
                    {
                        mMediaPlayer.Start();
                    }
                    break;
                case ACTION_PREVIOUS:
                    if (mMediaPlayer != null)
                    {
                        previous();
                    }
                    break;
                case ACTION_NEXT:
                    if (mMediaPlayer != null)
                    {
                        next();
                    }
                    break;
            }
            return StartCommandResult.NotSticky;

        }
        private void release()
        {
            if (mMediaPlayer != null)
                mMediaPlayer.Release();
        }

        private void playQueue()
        {
            release();
            File f = new File(filesQueue[currentlyPlaying].fileName);
            Android.Net.Uri musicUri = Android.Net.Uri.FromFile(f);
            mMediaPlayer = new MediaPlayer();
            try
            {
                mMediaPlayer.SetDataSource(BaseContext, musicUri);
            }
            catch (IOException e)
            {
                System.Console.WriteLine(e);
            }
            mMediaPlayer.SetOnPreparedListener(this);
            mMediaPlayer.SetOnCompletionListener(this);
            mMediaPlayer.PrepareAsync();
        }

        public void queueDirectory(String directoryName)
        {
            currentlyPlaying = 0;
            File directory = new File(directoryName);
            String fileName;
            Java.Util.Regex.Pattern r = Java.Util.Regex.Pattern.Compile(FILE_PATTERN);

            File[] fList = directory.ListFiles();
            foreach (File file in fList)
            {
                if (file.IsFile)
                {
                    fileName = file.ToString();
                    Matcher m = r.Matcher(fileName);
                    if (m.Find())
                    {
                        filesQueue.Add(new Music(fileName));
                    }
                }
                else if (file.IsDirectory)
                {
                    queueDirectory(file.AbsolutePath);
                }
            }
        }

        public int getCurrentPosition()
        {
            return mMediaPlayer.CurrentPosition;
        }

        public int getDuration()
        {
            return mMediaPlayer.Duration;
        }

        public void OnPrepared(MediaPlayer mp)
        {
            mp.Start();
        }

        public void OnCompletion(MediaPlayer mp)
        {
            if (currentlyPlaying < filesQueue.Count)
            {
                currentlyPlaying++;
                playQueue();
            }
        }

        public void next()
        {
            if (currentlyPlaying < filesQueue.Count - 1)
            {
                currentlyPlaying++;
                playQueue();
            }
        }

        public void previous()
        {
            if (currentlyPlaying > 0 && filesQueue.Count > 0)
            {
                currentlyPlaying--;
                playQueue();
            }
        }

        public Boolean isPlaying()
        {
            if (mMediaPlayer == null)
            {
                return false;
            }
            else
            {
                return mMediaPlayer.IsPlaying;
            }
        }

        public Boolean isStarted()
        {
            if (mMediaPlayer == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            release();
        }

        public override IBinder OnBind(Intent intent)
        {
            mBinder = new PlayerBinder(this);
            return mBinder;
        }

        public class PlayerBinder : Binder
        {
            MusicPlayerService service;
            public PlayerBinder(MusicPlayerService service)
            {
                this.service = service;
            }

            public MusicPlayerService getService()
            {
                return service;
            }
        }
    };



}