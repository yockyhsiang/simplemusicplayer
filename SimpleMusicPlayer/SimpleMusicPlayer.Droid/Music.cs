using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Media;
using Android.Graphics;

namespace SimpleMusicPlayer.Droid
{
    public class Music
    {
        public String title;
        public String album;
        public String fileName;
        public Bitmap songImage;

        /**
         * Constructor using title and album
         * @param title
         * @param album
         */
        public Music(String title, String album)
        {
            this.title = title;
            this.album = album;
        }

        /**
         * Constructor using filename
         * @param fileName file path and name
         */
        public Music(String fileName)
        {
            this.fileName = fileName;
            // TODO: this seems to be running in the UI thread and slow with hundreds of files
            MediaMetadataRetriever retriever = new MediaMetadataRetriever();

            retriever.SetDataSource(fileName);
            title = retriever.ExtractMetadata(MetadataKey.Title);
            album = retriever.ExtractMetadata(MetadataKey.Album);
            byte[] art = retriever.GetEmbeddedPicture();
            if (art != null)
            {
                songImage = BitmapFactory.DecodeByteArray(art, 0, art.Length);
            }
            retriever.Release();
        }
    }
}