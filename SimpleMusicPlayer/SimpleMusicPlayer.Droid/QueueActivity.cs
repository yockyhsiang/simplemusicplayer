using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;

namespace SimpleMusicPlayer.Droid
{
    [Activity(Label = "SimpleMusicPlayer.Droid", Icon = "@drawable/icon", Theme = "@style/MyTheme")]
    public class QueueActivity : Activity
    {

        private Button backtoNowPlaying;

        private static RecyclerView rv;
        private static List<Music> musicList;

        private static MusicPlayerService mPlayerService;
        private static Boolean mBound;

        ServiceConnection mConnection;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Queue);
            mConnection = new ServiceConnection(this);

            backtoNowPlaying = (Button)FindViewById(Resource.Id.backto_nowplaying);
            backtoNowPlaying.Click += delegate
            {
                Intent i = new Intent(this, typeof(MainActivity));
                StartActivity(i);
            };


        }

        protected override void OnStart()
        {
            base.OnStart();
            Intent i = new Intent(this, typeof(MusicPlayerService));
            BindService(i, mConnection, Bind.AutoCreate);
        }

        protected override void OnStop()
        {
            base.OnStop();
            if (mBound)
            {
                UnbindService(mConnection);
                mBound = false;
            }
        }

        internal class ServiceConnection : Java.Lang.Object, IServiceConnection
        {
            Activity _activity;

            public ServiceConnection(Activity activity)
            {
                _activity = activity;
            }

            public void OnServiceConnected(ComponentName name, IBinder service)
            {
                MusicPlayerService.PlayerBinder binder = (MusicPlayerService.PlayerBinder)service;
                mPlayerService = binder.getService();
                mBound = true;

                // Load data into RecyclerView
                initializeData();
            }

            public void OnServiceDisconnected(ComponentName name)
            {
                mBound = false;
            }

            public void initializeData()
            {
                musicList = new List<Music>();
                musicList = mPlayerService.filesQueue;

                rv = _activity.FindViewById<RecyclerView>(Resource.Id.rv);
                LinearLayoutManager llm = new LinearLayoutManager(_activity);
                rv.SetLayoutManager(llm);
                MusicRVAdapter adapter = new MusicRVAdapter(musicList);
                rv.SetAdapter(adapter);                
            }
        }
    }
}