using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace SimpleMusicPlayer.Droid
{
    class MusicRVAdapter : RecyclerView.Adapter
    {
        List<Music> musicList;
        public MusicRVAdapter(List<Music> musicList)
        {
            this.musicList = musicList;
        }

        public override int ItemCount
        {
            get
            {
                return musicList.Count;
            }
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            MusicViewHolder myholder = (MusicViewHolder)holder;
            myholder.title.Text = (musicList[position].title);
            myholder.album.Text = (musicList[position].album);
            if (musicList[position].songImage != null)
                myholder.songImage.SetImageBitmap(musicList[position].songImage);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View v = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.queue_item, parent, false);
            return new MusicViewHolder(v);
        }

        public class MusicViewHolder : RecyclerView.ViewHolder
        {
            public CardView cv;
            public TextView title;
            public TextView album;
            public ImageView songImage;

            public MusicViewHolder(View itemView) : base(itemView)
            {
                cv = (CardView)itemView.FindViewById(Resource.Id.card_view);
                title = (TextView)itemView.FindViewById(Resource.Id.song_title);
                album = (TextView)itemView.FindViewById(Resource.Id.song_album);
                songImage = (ImageView)itemView.FindViewById(Resource.Id.song_image);
            }
        }
    }
}