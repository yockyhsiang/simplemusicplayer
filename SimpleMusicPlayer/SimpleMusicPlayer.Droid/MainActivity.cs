﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using System.IO;
using Android.Content.Res;
using Android.Preferences;

namespace SimpleMusicPlayer.Droid
{
    [Activity(Label = "SimpleMusicPlayer", MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/MyTheme")]
    public class MainActivity : Activity
    {
        private ImageButton previousButton;
        private ImageButton playButton;
        private ImageButton nextButton;
        private TextView currentFolderText;
        private Button showQueue;
        private Button chooseButton;

        private static MusicPlayerService mPlayerService;
        private static Boolean mBound = false;

        private ServiceConnection mConnection = new ServiceConnection();



        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it


            previousButton = (ImageButton)FindViewById(Resource.Id.previous);
            playButton = (ImageButton)FindViewById(Resource.Id.play);
            nextButton = (ImageButton)FindViewById(Resource.Id.next);
            currentFolderText = (TextView)FindViewById(Resource.Id.current_folder);
            showQueue = (Button)FindViewById(Resource.Id.show_queue);
            chooseButton = (Button)FindViewById(Resource.Id.choose_folder);

            ISharedPreferences preferences = PreferenceManager.GetDefaultSharedPreferences(this);
            ISharedPreferencesEditor editor = preferences.Edit();

            string str = preferences.GetString("dir", "?");
            int playBtn = preferences.GetInt("playBtn", Resource.Drawable.ic_media_play);
            currentFolderText.Text = str;

            playButton.SetImageResource(playBtn);

            playButton.Click += delegate
                 {
                     if (mPlayerService.isStarted())
                     {
                         if (mPlayerService.isPlaying())
                         {
                             Intent i = new Intent(this, typeof(MusicPlayerService));
                             i.SetAction(MusicPlayerService.ACTION_PAUSE);
                             StartService(i);
                             playButton.SetImageResource(Resource.Drawable.ic_media_play);
                         }
                         else
                         {
                             Intent i = new Intent(this, typeof(MusicPlayerService));
                             i.SetAction(MusicPlayerService.ACTION_RESUME);
                             StartService(i);
                             playButton.SetImageResource(Resource.Drawable.ic_media_pause);
                         }
                     }
                     else
                     {
                         Intent i = new Intent(this, typeof(MusicPlayerService));
                         i.SetAction(MusicPlayerService.ACTION_PLAY);
                         StartService(i);
                         playButton.SetImageResource(Resource.Drawable.ic_media_pause);
                     }
                 };



            nextButton.Click += delegate
            {
                if (mPlayerService.isStarted())
                {
                    Intent i = new Intent(this, typeof(MusicPlayerService));
                    i.SetAction(MusicPlayerService.ACTION_NEXT);
                    StartService(i);
                }
            };

            previousButton.Click += delegate
            {
                Intent i = new Intent(this, typeof(MusicPlayerService));
                i.SetAction(MusicPlayerService.ACTION_PREVIOUS);
                StartService(i);
            };

            showQueue.Click += delegate
            {
                if (mPlayerService.isPlaying())
                    editor.PutInt("playBtn", Resource.Drawable.ic_media_pause);
                else
                    editor.PutInt("playBtn", Resource.Drawable.ic_media_play);
                editor.Commit();

                Intent intent = new Intent(this, typeof(QueueActivity));
                StartActivity(intent);
            };



            chooseButton.Click += delegate
            {
                //load the music directory path:
                var dir = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryMusic).Path;
                playDirectory(dir);

                editor.PutString("dir", dir);
                editor.Commit();
            };


        }
        protected override void OnStart()
        {
            base.OnStart();
            Intent i = new Intent(this, typeof(MusicPlayerService));
            BindService(i, mConnection, Bind.AutoCreate);
        }

        protected override void OnStop()
        {
            base.OnStop();
            if (mBound)
            {
                UnbindService(mConnection);
                mBound = false;
            }
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

        }

        private void playDirectory(String dir)
        {
            currentFolderText.Text = dir;
            Intent i = new Intent(this, typeof(MusicPlayerService));
            i.SetAction(MusicPlayerService.ACTION_PLAY_DIRECTORY);
            i.PutExtra("directory", dir);
            StartService(i);
            playButton.SetImageResource(Resource.Drawable.ic_media_pause);
        }


        private class ServiceConnection : Java.Lang.Object, IServiceConnection
        {
            public void OnServiceConnected(ComponentName name, IBinder service)
            {
                MusicPlayerService.PlayerBinder binder = (MusicPlayerService.PlayerBinder)service;
                mPlayerService = binder.getService();
                mBound = true;
            }

            public void OnServiceDisconnected(ComponentName name)
            {
                mBound = false;
            }
        }

    }
}


